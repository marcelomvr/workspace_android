package com.example.copiarcolar;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button copiar = (Button) findViewById(R.id.btnCopiar);
		final TextView destino = (TextView) findViewById(R.id.txtDestino);
		final TextView origem = (TextView) findViewById(R.id.origem);

		copiar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Log.i("OlaMundo", "Botao Clicado");
				destino.setText(origem.getText());

			}
		});

	}

}
